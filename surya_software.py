## Steps for running the script - 
## 1. Install grequests by running - pip install grequests
## 2. Install requests by running - pip install requests
## 3. Run the script - python surya_software.py

import grequests
import requests
import math

total_hits = 100
concurrency = 10
url = 'http://surya-interview.appspot.com/message'
time_elapsed = []

def get_urls():
    """This method composes list of urls based on concurrency of load testing"""

    rs = []
    headers={'X-Surya-Email-Id':'bikash8gupta@gmail.com'}
    for i in range(concurrency):
	rs.append(grequests.get(url, headers = headers, 
	    hooks = dict(response = request_callback_function)))
    return rs

def request_callback_function(resp, *args, **kwargs):
    """This is the callback function to be executed on each successful request"""

    r = requests.post(url, data=str(resp.content))
    time_elapsed.append((r.elapsed + resp.elapsed).total_seconds())

def analyse_performance():
    """This method executes the url and calculated percentile, mean and standard deviation"""

    for i in range(total_hits/concurrency):
	print "Hitting the url with concurrency %s" %concurrency
	rs = get_urls()
	grequests.map(rs)
    time_elapsed.sort()
    print "Tenth percentile: %s" %time_elapsed[9]
    print "50th percentile: %s" %time_elapsed[49]
    print "90th percentile: %s" %time_elapsed[89]
    print "95th percentile: %s" %time_elapsed[94]
    print "99th percentile: %s" %time_elapsed[98]

    total_elapsed_time = sum(time_elapsed)
    mean = total_elapsed_time/100
    print "Mean: %s" %(mean)

    standard_deviation = math.sqrt(sum([math.pow(t-mean, 2) for t in time_elapsed])/100)

    print "Standard Deviation: %s" %standard_deviation 

if __name__ == '__main__':
    print "Executing urls. Please wait..."
    analyse_performance()
    
